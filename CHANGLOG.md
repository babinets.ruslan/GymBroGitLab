# Changelog

## [0.0.1] - 2024-05-23
### Added
- Initial release of GymBro with basic functionality for tracking workouts.
